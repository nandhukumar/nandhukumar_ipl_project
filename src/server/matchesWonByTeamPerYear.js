function matchesWonByTeamPerYear(matchData) {
  let matchDataPerTeam = {};
  for (let data of matchData) {
    let teamName = data.winner;
    if (!teamName) {
      teamName = "Match Tie";
    }
    let year = data.season;

    if (!matchDataPerTeam.hasOwnProperty(teamName)) {
      matchDataPerTeam[teamName] = {};
      matchDataPerTeam[teamName][year] = 1;
    } else {
      if (matchDataPerTeam[teamName][year] !== undefined) {
        matchDataPerTeam[teamName][year] += 1;
      } else {
        matchDataPerTeam[teamName][year] = 1;
      }
    }
  }
  return matchDataPerTeam;
}

module.exports = matchesWonByTeamPerYear;
