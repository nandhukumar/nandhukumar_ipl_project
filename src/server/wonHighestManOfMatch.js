function wonHighestManOfMatch(matchData) {
  let manOfMatchPerYear = {};

  for (let match of matchData) {
    let year = match.season;
    let playerOfMatch = match.player_of_match;

    if (!manOfMatchPerYear.hasOwnProperty(year)) {
      manOfMatchPerYear[year] = {};
      manOfMatchPerYear[year][playerOfMatch] = 1;
    } else {
      if (manOfMatchPerYear[year].hasOwnProperty(playerOfMatch)) {
        manOfMatchPerYear[year][playerOfMatch] += 1;
      } else {
        manOfMatchPerYear[year][playerOfMatch] = 1;
      }
    }
  }

  let highestManOfMatchPerYear = {};

  for (let eachYear in manOfMatchPerYear) {
    let eachPlayersCountPerYear = Object.entries(manOfMatchPerYear[eachYear]);

    eachPlayersCountPerYear.sort(
      (firstVal, secondVal) => secondVal[1] - firstVal[1]
    );

    let sortedPlayerPerYear = Object.fromEntries([eachPlayersCountPerYear[0]]);
    highestManOfMatchPerYear[eachYear] = sortedPlayerPerYear;
  }

  return highestManOfMatchPerYear;
}

module.exports = wonHighestManOfMatch;
