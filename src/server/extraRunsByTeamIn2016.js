function extraRunsByTeamIn2016(matchData, deliveryData, year) {
  let extraRunsConcdededTeam = {};

  for (let eachMatch of matchData) {
    let matchId = eachMatch.id;
    let matchYear = eachMatch.season;

    if (matchYear == year) {
      for (let eachDelivery of deliveryData) {
        let deliveryId = eachDelivery.match_id;
        let teamName = eachDelivery.bowling_team;
        let extraRuns = eachDelivery.extra_runs;

        if (
          matchId === deliveryId &&
          extraRunsConcdededTeam.hasOwnProperty(teamName)
        ) {
          extraRunsConcdededTeam[teamName] += parseInt(extraRuns);
        } else if (
          matchId === deliveryId &&
          !extraRunsConcdededTeam.hasOwnProperty(teamName)
        ) {
          extraRunsConcdededTeam[teamName] = parseInt(extraRuns);
        }
      }
    }
  }
  return extraRunsConcdededTeam;
}

module.exports = extraRunsByTeamIn2016;
