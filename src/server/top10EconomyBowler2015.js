function top10EconomyBowler2015(matchData, deliveryData, year) {
  let top10EconomyBowler = {};
  for (let eachMatch of matchData) {
    let matchId = eachMatch.id;
    let matchYear = eachMatch.season;

    if (matchYear == year) {
      for (let eachDelivery of deliveryData) {
        let deliveryId = eachDelivery.match_id;
        let bowler = eachDelivery.bowler;
        let countOfBall = parseInt(eachDelivery.ball);
        let concededRuns =
          parseInt(eachDelivery.wide_runs) +
          parseInt(eachDelivery.noball_runs) +
          parseInt(eachDelivery.batsman_runs);

        if (
          matchId === deliveryId &&
          top10EconomyBowler.hasOwnProperty(bowler)
        ) {
          if (countOfBall > 6) {
            top10EconomyBowler[bowler]["runsConceded"] += concededRuns;
          } else if (countOfBall < 7) {
            top10EconomyBowler[bowler]["ballCount"] += 1;
            top10EconomyBowler[bowler]["runsConceded"] += concededRuns;
          }
        } else if (
          matchId === deliveryId &&
          !top10EconomyBowler.hasOwnProperty(bowler)
        ) {
          top10EconomyBowler[bowler] = {};
          top10EconomyBowler[bowler]["name"] = bowler;
          if (countOfBall > 6) {
            top10EconomyBowler[bowler]["runsConceded"] = concededRuns;
          } else if (countOfBall < 7) {
            top10EconomyBowler[bowler]["ballCount"] = 1;
            top10EconomyBowler[bowler]["runsConceded"] = concededRuns;
          }
        }
      }
    }
  }
  let arrayOfTopEconomyBowlers = [];

  for (eachBowler in top10EconomyBowler) {
    let totalRuns = top10EconomyBowler[eachBowler].runsConceded;
    let oversCount = (top10EconomyBowler[eachBowler].ballCount / 6).toFixed(2);
    let economy = (totalRuns / oversCount).toFixed(2);

    top10EconomyBowler[eachBowler]["economy"] = parseFloat(economy);
    arrayOfTopEconomyBowlers.push(top10EconomyBowler[eachBowler]);
  }

  arrayOfTopEconomyBowlers.sort((a, b) => a.economy - b.economy);

  let arrayOfTop10EconomyBowlers = arrayOfTopEconomyBowlers.slice(0, 10);

  return arrayOfTop10EconomyBowlers;
}

module.exports = top10EconomyBowler2015;
