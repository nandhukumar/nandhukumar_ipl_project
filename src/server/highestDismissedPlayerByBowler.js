function highestDismissedPlayerByBowler(deliveryData) {
  let dismissalNotInBowlerCredit = [
    "run out",
    "retired hurt",
    "obstructing the field",
  ];

  let dismissalOfBatsmanAndBowlerCount = {};

  for (delivery of deliveryData) {
    let dismissalType = delivery.dismissal_kind;
    let batsmanName = delivery.batsman;
    let bowlerName = delivery.bowler;
    if (!(dismissalType in dismissalNotInBowlerCredit) && dismissalType) {
      if (
        `${batsmanName} - ${bowlerName}` in dismissalOfBatsmanAndBowlerCount
      ) {
        dismissalOfBatsmanAndBowlerCount[`${batsmanName} - ${bowlerName}`] += 1;
      } else {
        dismissalOfBatsmanAndBowlerCount[`${batsmanName} - ${bowlerName}`] = 1;
      }
    } else {
      continue;
    }
  }

  let sortingDismissalOfBatsmanAndBowlerCount = Object.entries(
    dismissalOfBatsmanAndBowlerCount
  );
  sortingDismissalOfBatsmanAndBowlerCount.sort(
    (firstVal, secondVal) => secondVal[1] - firstVal[1]
  );
  let sortedDismissalOfBatsmanAndBowlerCount = Object.fromEntries([
    sortingDismissalOfBatsmanAndBowlerCount[0],
  ]);

  return sortedDismissalOfBatsmanAndBowlerCount;
}

module.exports = highestDismissedPlayerByBowler;
