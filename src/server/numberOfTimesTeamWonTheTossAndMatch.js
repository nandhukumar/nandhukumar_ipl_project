function numberOfTimesTeamWonTheTossAndMatch(matchData) {
  let teamWonCount = {};

  for (let match of matchData) {
    let tossWonTeam = match.toss_winner;
    let matchWinner = match.winner;

    if (tossWonTeam in teamWonCount && matchWinner === tossWonTeam) {
      teamWonCount[matchWinner] += 1;
    } else if (!(tossWonTeam in teamWonCount) && matchWinner === tossWonTeam) {
      teamWonCount[matchWinner] = 1;
    } else {
      continue;
    }
  }

  let arrayTeamWonCount = Object.entries(teamWonCount);
  arrayTeamWonCount.sort((a, b) => b[1] - a[1]);
  let sortedTeamWonCount = Object.fromEntries(arrayTeamWonCount);
  return sortedTeamWonCount;
}

module.exports = numberOfTimesTeamWonTheTossAndMatch;
