function strikeRateOfBatsmanPerYear(matchData, deliveryData) {
  let batsmanDataPerYear = {};

  for (let delivery of deliveryData) {
    let deliveryId = delivery.match_id;
    let batsmanName = delivery.batsman;
    let runScored = parseInt(delivery.batsman_runs);
    let wideBall = parseInt(delivery.wide_runs);

    for (let match of matchData) {
      let matchId = match.id;
      let matchYear = match.season;

      if (matchId === deliveryId) {
        if (!batsmanDataPerYear.hasOwnProperty(batsmanName)) {
          batsmanDataPerYear[batsmanName] = {};
          batsmanDataPerYear[batsmanName][matchYear] = {
            runScored: 0,
            noOfBalls: 0,
          };
          batsmanDataPerYear[batsmanName][matchYear]["runScored"] += runScored;

          if (wideBall === 0) {
            batsmanDataPerYear[batsmanName][matchYear]["noOfBalls"] += 1;
          }
        } else if (batsmanDataPerYear.hasOwnProperty(batsmanName)) {
          if (batsmanDataPerYear[batsmanName].hasOwnProperty(matchYear)) {
            batsmanDataPerYear[batsmanName][matchYear]["runScored"] +=
              runScored;
            if (wideBall === 0) {
              batsmanDataPerYear[batsmanName][matchYear]["noOfBalls"] += 1;
            }
          } else if (
            !batsmanDataPerYear[batsmanName].hasOwnProperty(matchYear)
          ) {
            batsmanDataPerYear[batsmanName][matchYear] = {
              runScored: 0,
              noOfBalls: 0,
            };
            batsmanDataPerYear[batsmanName][matchYear]["runScored"] +=
              runScored;
            if (wideBall === 0) {
              batsmanDataPerYear[batsmanName][matchYear]["noOfBalls"] = 1;
            }
          }
        }
      }
    }
  }

  let arrayOfStrikeRateOfBatsmanPerYear = [];
  let eachPlayerStrikeRate = {};

  for (let eachPlayer in batsmanDataPerYear) {
    let eachPlayerByYear = {};

    for (let eachYear in batsmanDataPerYear[eachPlayer]) {
      eachPlayerByYear[eachYear] = parseFloat(
        (
          (batsmanDataPerYear[eachPlayer][eachYear]["runScored"] /
            batsmanDataPerYear[eachPlayer][eachYear]["noOfBalls"]) *
          100
        ).toFixed(2)
      );
    }
    eachPlayerStrikeRate[eachPlayer] = eachPlayerByYear;
  }

  let convertingObjToArray = Object.entries(eachPlayerStrikeRate);
  convertingObjToArray.sort();
  let sortedEachPlayerStrikeRate = Object.fromEntries(convertingObjToArray);

  return sortedEachPlayerStrikeRate;
}

module.exports = strikeRateOfBatsmanPerYear;

