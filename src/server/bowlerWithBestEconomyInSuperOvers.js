function bowlerWithBestEconomyInSuperOvers(matchData, deliveryData) {
  let bowlerEconomyInSuperOvers = {};

  for (let eachDelivery of deliveryData) {
    let deliveryId = eachDelivery.match_id;
    let bowler = eachDelivery.bowler;
    let isSuperOver = parseInt(eachDelivery.is_super_over);
    let countOfBall = parseInt(eachDelivery.ball);
    let concededRuns =
      parseInt(eachDelivery.wide_runs) +
      parseInt(eachDelivery.noball_runs) +
      parseInt(eachDelivery.batsman_runs);

    if (isSuperOver !== 0) {
      for (let eachMatch of matchData) {
        let matchId = eachMatch.id;
        let matchYear = eachMatch.season;

        if (
          matchId === deliveryId &&
          bowlerEconomyInSuperOvers.hasOwnProperty(bowler)
        ) {
          if (countOfBall > 6) {
            bowlerEconomyInSuperOvers[bowler]["runsConceded"] += concededRuns;
          } else if (countOfBall < 7) {
            bowlerEconomyInSuperOvers[bowler]["ballCount"] += 1;
            bowlerEconomyInSuperOvers[bowler]["runsConceded"] += concededRuns;
          }
        } else if (
          matchId === deliveryId &&
          !bowlerEconomyInSuperOvers.hasOwnProperty(bowler)
        ) {
          bowlerEconomyInSuperOvers[bowler] = {
            name: "",
            runsConceded: 0,
            ballCount: 0,
          };
          bowlerEconomyInSuperOvers[bowler]["name"] = bowler;
          if (countOfBall > 6) {
            bowlerEconomyInSuperOvers[bowler]["runsConceded"] = concededRuns;
          } else if (countOfBall < 7) {
            bowlerEconomyInSuperOvers[bowler]["ballCount"] += 1;
            bowlerEconomyInSuperOvers[bowler]["runsConceded"] += concededRuns;
          }
        }
      }
    }
  }
  let arrayOfTopEconomyBowlers = [];

  for (eachBowler in bowlerEconomyInSuperOvers) {
    let totalRuns = bowlerEconomyInSuperOvers[eachBowler].runsConceded;
    let oversCount = (
      bowlerEconomyInSuperOvers[eachBowler].ballCount / 6
    ).toFixed(2);
    let economy = (totalRuns / oversCount).toFixed(2);

    bowlerEconomyInSuperOvers[eachBowler]["economy"] = parseFloat(economy);
    arrayOfTopEconomyBowlers.push(bowlerEconomyInSuperOvers[eachBowler]);
  }

  arrayOfTopEconomyBowlers.sort((a, b) => a.economy - b.economy);

  let arrayOfTopEconomyBowlersInSuperOver = arrayOfTopEconomyBowlers[0];

  return arrayOfTopEconomyBowlersInSuperOver;
}

module.exports = bowlerWithBestEconomyInSuperOvers;
