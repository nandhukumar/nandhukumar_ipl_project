function matchesPlayedPerYear(matchData) {
  let yearWiseData = {};
  for (let data of matchData) {
    let year = data.season;
    if (year in yearWiseData) {
      yearWiseData[year] = yearWiseData[year] + 1;
    } else {
      yearWiseData[year] = 1;
    }
  }
  return yearWiseData;
}

module.exports = matchesPlayedPerYear;
