const csv = require("csvtojson");

const fs = require("fs");

const matches = "../data/matches.csv";
const deliveries = "../data/deliveries.csv";

const matchesPlayedPerYear = require("./matchesPlayedPerYear");
const matchesWonByTeamPerYear = require("./matchesWonByTeamPerYear");
const extraRunsByTeamIn2016 = require("./extraRunsByTeamIn2016");
const top10EconomyBowler2015 = require("./top10EconomyBowler2015");
const numberOfTimesTeamWonTheTossAndMatch = require("./numberOfTimesTeamWonTheTossAndMatch.js");
const wonHighestManOfMatch = require("./wonHighestManOfMatch.js");
const strikeRateOfBatsmanPerYear = require("./strikeRateOfBatsmanPerYear.js");
const bowlerWithBestEconomyInSuperOvers = require("./bowlerWithBestEconomyInSuperOvers.js");
const highestDismissedPlayerByBowler = require("./highestDismissedPlayerByBowler.js");

csv()
  .fromFile(matches)
  .then((matchData) => {
    let matchesPlayedPerYearResult = matchesPlayedPerYear(matchData);

    matchesPlayedPerYearResult = JSON.stringify(matchesPlayedPerYearResult);

    fs.writeFile(
      "../public/output/matchesPlayedPerYearResult.json",
      matchesPlayedPerYearResult,
      (err) => {
        if (err) console.log(err);
        else {
          console.log("File written successfully\n");
        }
      }
    );

    let matchesWonByTeamPerYearResult = matchesWonByTeamPerYear(matchData);
    matchesWonByTeamPerYearResult = JSON.stringify(
      matchesWonByTeamPerYearResult
    );

    fs.writeFile(
      "../public/output/matchesWonByTeamPerYearResult.json",
      matchesWonByTeamPerYearResult,
      (err) => {
        if (err) console.log(err);
        else {
          console.log("File written successfully\n");
        }
      }
    );

    let numberOfTimesTeamWonTheTossAndMatchResult =
      numberOfTimesTeamWonTheTossAndMatch(matchData);

    numberOfTimesTeamWonTheTossAndMatchResult = JSON.stringify(
      numberOfTimesTeamWonTheTossAndMatchResult
    );

    fs.writeFile(
      "../public/output/numberOfTimesTeamWonTheTossAndMatchResult.json",
      numberOfTimesTeamWonTheTossAndMatchResult,
      (err) => {
        if (err) console.log(err);
        else {
          console.log("File written successfully\n");
        }
      }
    );

    let wonHighestManOfMatchResult = wonHighestManOfMatch(matchData);

    wonHighestManOfMatchResult = JSON.stringify(wonHighestManOfMatchResult);

    fs.writeFile(
      "../public/output/wonHighestManOfMatchResult.json",
      wonHighestManOfMatchResult,
      (err) => {
        if (err) console.log(err);
        else {
          console.log("File written successfully\n");
        }
      }
    );

    csv()
      .fromFile(deliveries)
      .then((deliveryData) => {
        let extraRunsByTeamIn2016Result = extraRunsByTeamIn2016(
          matchData,
          deliveryData,
          2016
        );

        extraRunsByTeamIn2016Result = JSON.stringify(
          extraRunsByTeamIn2016Result
        );

        fs.writeFile(
          "../public/output/extraRunsByTeamIn2016Result.json",
          extraRunsByTeamIn2016Result,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );

        let top10EconomyBowler2015Result = top10EconomyBowler2015(
          matchData,
          deliveryData,
          2015
        );

        top10EconomyBowler2015Result = JSON.stringify(
          top10EconomyBowler2015Result
        );

        fs.writeFile(
          "../public/output/top10EconomyBowler2015.json",
          top10EconomyBowler2015Result,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );

        let strikeRateOfBatsmanPerYearResult = strikeRateOfBatsmanPerYear(
          matchData,
          deliveryData
        );

        strikeRateOfBatsmanPerYearResult = JSON.stringify(
          strikeRateOfBatsmanPerYearResult
        );

        fs.writeFile(
          "../public/output/strikeRateOfBatsmanPerYearResult.json",
          strikeRateOfBatsmanPerYearResult,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );

        let bowlerWithBestEconomyInSuperOversResult =
          bowlerWithBestEconomyInSuperOvers(matchData, deliveryData);

        bowlerWithBestEconomyInSuperOversResult = JSON.stringify(
          bowlerWithBestEconomyInSuperOversResult
        );

        fs.writeFile(
          "../public/output/bowlerWithBestEconomyInSuperOversResult.json",
          bowlerWithBestEconomyInSuperOversResult,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );

        let highestDismissedPlayerByBowlerResult =
          highestDismissedPlayerByBowler(deliveryData);

        highestDismissedPlayerByBowlerResult = JSON.stringify(
          highestDismissedPlayerByBowlerResult
        );

        fs.writeFile(
          "../public/output/highestDismissedPlayerByBowlerResult.json",
          highestDismissedPlayerByBowlerResult,
          (err) => {
            if (err) console.log(err);
            else {
              console.log("File written successfully\n");
            }
          }
        );
      });
  });
